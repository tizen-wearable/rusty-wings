import { AmbientLight, Color } from "three";

export const ambientLight = new AmbientLight(
  new Color("rgb(220, 136, 116)"),
  0.5
);
