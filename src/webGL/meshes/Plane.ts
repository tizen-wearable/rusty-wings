import {
  PlaneGeometry,
  MeshBasicMaterial,
  Mesh,
  Color,
  Vector2,
  BufferAttribute,
} from "three";

import { clock } from "@/webGL/objects";

import { ImprovedNoise } from "three/examples/jsm/math/ImprovedNoise";

export class Plane extends Mesh {
  constructor({ color }: { color: string }) {
    const planeGeometry = new PlaneGeometry(10, 10, 15, 15);

    planeGeometry.rotateX(Math.PI * -0.5);

    const meshPhongMaterial = new MeshBasicMaterial({
      color: new Color(color),
      transparent: true,
      opacity: 0.6,
    });

    super(planeGeometry, meshPhongMaterial);
  }

  moveVertices = () => {
    const position = this.geometry.attributes.position;
    const uv = this.geometry.attributes.uv;

    const improvedNoise = new ImprovedNoise();
    const vector2 = new Vector2();

    const elapsedTime = clock.getElapsedTime();

    for (let i = 0; i < position.count; i++) {
      if (uv instanceof BufferAttribute) {
        vector2.fromBufferAttribute(uv, i).multiplyScalar(50);
      }

      const y = improvedNoise.noise(
        vector2.x,
        vector2.y + elapsedTime,
        elapsedTime * 0.2
      );

      position.setY(i, y);
    }

    position.needsUpdate = true;
  };
}
