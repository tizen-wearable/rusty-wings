import {
  ambientLight,
  hemisphereLight,
  scene,
  shadowLight,
} from "@/webGL/objects";

export default function () {
  const createLights = () => {
    shadowLight.position.set(150, 350, 350);

    shadowLight.castShadow = true;

    shadowLight.shadow.camera.left = -400;
    shadowLight.shadow.camera.right = 400;
    shadowLight.shadow.camera.top = 400;
    shadowLight.shadow.camera.bottom = -400;
    shadowLight.shadow.camera.near = 1;
    shadowLight.shadow.camera.far = 1000;

    shadowLight.shadow.mapSize.width = 2048;
    shadowLight.shadow.mapSize.height = 2048;

    scene.add(ambientLight);
    scene.add(hemisphereLight);
    scene.add(shadowLight);
  };

  return {
    createLights,
  };
}
