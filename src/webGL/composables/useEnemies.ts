import { scene, bomb } from "@/webGL/objects";

export default function () {
  const createBomb = () => {
    bomb.loadModel().then((bombGroup) => {
      bombGroup.position.y = 1.5;
      bombGroup.position.x = 1.5;
      bombGroup.rotateZ((5 * Math.PI) / 6);
      bombGroup.scale.set(0.1, 0.1, 0.1);
    });

    scene.add(bomb);
  };

  return {
    createBomb,
  };
}
