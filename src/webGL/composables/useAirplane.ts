import { airplane, scene } from "@/webGL/objects";

export default function () {
  const createAirplane = () => {
    airplane.loadModel().then((airplaneGroup) => {
      airplaneGroup.position.y = 2;
      airplaneGroup.rotateY(Math.PI * 0.5);
      airplaneGroup.scale.set(0.2, 0.2, 0.2);

      airplane.propeller.loadModel().then((propellerGroup) => {
        propellerGroup.position.y = 2;
        propellerGroup.rotateY(Math.PI * 0.5);
        propellerGroup.scale.set(0.2, 0.2, 0.2);
      });
    });

    scene.add(airplane);
  };

  return {
    createAirplane,
  };
}
