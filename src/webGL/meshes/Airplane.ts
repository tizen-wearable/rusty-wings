import { AbstractGLBModel } from "@/webGL/meshes/base";

import { Propeller } from "@/webGL/meshes";

export class Airplane extends AbstractGLBModel {
  propeller = new Propeller({
    name: "Propeller",
  });

  constructor({ name }: { name: string }) {
    super({ name });

    this.add(this.propeller);
  }
}
