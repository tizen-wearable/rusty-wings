import { HEIGHT, WIDTH } from "@/webGL/constants";

import {
  sea,
  airplane,
  fog,
  perspectiveCamera,
  scene,
  webGLRenderer,
} from "@/webGL/objects";

export default function () {
  const createScene = () => {
    const container = document.getElementById("webGL");

    scene.fog = fog;

    perspectiveCamera.position.x = 0;
    perspectiveCamera.position.z = 9;
    perspectiveCamera.position.y = 2;

    webGLRenderer.setSize(WIDTH, HEIGHT);
    webGLRenderer.shadowMap.enabled = true;

    container && container.appendChild(webGLRenderer.domElement);
  };

  const clearScene = () => {
    scene.clear();
  };

  const loop = () => {
    sea.moveVertices();
    airplane.propeller.moveBlades();

    webGLRenderer.render(scene, perspectiveCamera);

    requestAnimationFrame(loop);
  };

  return {
    loop,
    clearScene,
    createScene,
  };
}
