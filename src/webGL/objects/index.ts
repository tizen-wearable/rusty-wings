export { sea } from "@/webGL/objects/sea";
export { airplane } from "@/webGL/objects/airplane";
export { bomb } from "@/webGL/objects/bomb";
export { fog } from "@/webGL/objects/fog";

export { webGLRenderer } from "@/webGL/objects/base/webGLRenderer";
export { perspectiveCamera } from "@/webGL/objects/base/perspectiveCamera";
export { gltfLoader } from "@/webGL/objects/base/gltfLoader";
export { scene } from "@/webGL/objects/base/scene";
export { orbitControls } from "@/webGL/objects/base/orbitControls";
export { clock } from "@/webGL/objects/base/clock";

export { hemisphereLight } from "@/webGL/objects/lights/hemisphereLight";
export { shadowLight } from "@/webGL/objects/lights/shadowLight";
export { ambientLight } from "@/webGL/objects/lights/ambientLight";
