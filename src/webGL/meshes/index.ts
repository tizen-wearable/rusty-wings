export { Plane } from "@/webGL/meshes/Plane";
export { Bomb } from "@/webGL/meshes/Bomb";
export { Airplane } from "@/webGL/meshes/Airplane";
export { Propeller } from "@/webGL/meshes/Propeller";
