import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

import { perspectiveCamera, webGLRenderer } from "@/webGL/objects";

export const orbitControls = new OrbitControls(
  perspectiveCamera,
  webGLRenderer.domElement
);
