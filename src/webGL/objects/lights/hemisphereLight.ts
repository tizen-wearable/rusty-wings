import { Color, HemisphereLight } from "three";

export const hemisphereLight = new HemisphereLight(
  new Color("rgb(170, 170, 170)"),
  new Color("rgb(0, 0, 0)"),
  0.9
);
