import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

import BaseView from "@/ui/views/BaseView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "base",
    component: BaseView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
