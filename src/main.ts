import { createApp } from "vue";

import router from "@/ui/router";
import store from "@/ui/store";

import App from "@/ui/App.vue";

createApp(App).use(store).use(router).mount("#app");
