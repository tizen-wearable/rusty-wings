import { Color, DirectionalLight } from "three";

export const shadowLight = new DirectionalLight(
  new Color("rgb(255, 255, 255)"),
  0.9
);
