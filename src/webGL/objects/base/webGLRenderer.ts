import { WebGLRenderer } from "three";

export const webGLRenderer = new WebGLRenderer({
  alpha: true,
  antialias: true,
});
