import { Color, Fog } from "three";

export const fog = new Fog(new Color("rgb(247, 217, 170)"), 0, 30);
