import { Group } from "three";

import { gltfLoader } from "@/webGL/objects";

export abstract class AbstractGLBModel extends Group {
  name: string;

  protected constructor({ name }: { name: string }) {
    super();

    this.castShadow = true;
    this.receiveShadow = true;
    this.name = name;
  }

  loadModel = () => {
    return new Promise<Group>((resolve) => {
      gltfLoader.load(`${this.name}.glb`, ({ scene }) => {
        // scene.traverse((object3D) => {
        //   if (object3D instanceof Mesh) {
        //     object3D.castShadow = true;
        //     object3D.receiveShadow = true;
        //   }
        // });

        this.add(scene);

        resolve(scene);
      });
    });
  };
}
