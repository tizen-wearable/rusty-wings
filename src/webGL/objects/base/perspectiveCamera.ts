import { PerspectiveCamera } from "three";

import {
  FAR_PLANE,
  FIELD_OF_VIEW,
  HEIGHT,
  NEAR_PLANE,
  WIDTH,
} from "@/webGL/constants";

export const perspectiveCamera = new PerspectiveCamera(
  FIELD_OF_VIEW,
  WIDTH / HEIGHT,
  NEAR_PLANE,
  FAR_PLANE
);
