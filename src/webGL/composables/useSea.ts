import { sea, scene } from "@/webGL/objects";

export default function () {
  const createSea = () => {
    sea.receiveShadow = true;
    sea.castShadow = true;

    scene.add(sea);
  };

  return {
    createSea,
  };
}
