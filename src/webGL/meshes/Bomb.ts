import { AbstractGLBModel } from "@/webGL/meshes/base";

export class Bomb extends AbstractGLBModel {
  constructor({ name }: { name: string }) {
    super({ name });
  }
}
