export const FIELD_OF_VIEW = 60;
export const HEIGHT = window.innerHeight;
export const WIDTH = window.innerWidth;
export const FAR_PLANE = 10000;
export const NEAR_PLANE = 1;
